# Clone the repo
git clone https://alpha_admin@bitbucket.org/alpha_admin/mvc.git

# composer update
	> go to the cloned folder
		$ cd /var/www/html/mvc
		$ composer update

# create a demo mysql database for e.g "mvc"

# need to change configuration file according to your's
	> file location /mvc/app/core/config/main.php

# check for a demo db with a demo user table
	> file location /mvc/demo/db/mvc.sql
	> Import the same and you can use for a go.

# Now you are ready to run the application
e.g 
	http://localhost/mvc/public/home/index
	http://localhost/mvc/public/home/about
	http://localhost/mvc/public/home/contact
	http://localhost/mvc/public/user/list
	http://localhost/mvc/public/user/fetch/1


Note: You can also try setup your virtual host to access the web application.


