<?php
/*
 * Class Users
 * Model class file
 * 
 * @author Arghya Ghosh <arghya@capitalnumbers.com>
 * @date Sept 05, 2016
 */
use Illuminate\Database\Eloquent\Model as Eloquent;

class Users extends Eloquent
{
	protected $id;
	protected $email; 
	protected $username; 
	protected $password;  
	protected $firstname;  
	protected $lastname;  
	
	public $table = "user";
	protected $fillable = ['username', 'email'];

	/**
	 * constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * reset
	 *
	 * Reset values of all member variables.
	 */
	public function reset() {
		$this->id = null;
		$this->email = null;
		$this->username = null;
		$this->password = null;
		$this->firstname = null;
		$this->lastname = null;
	}
	
	public function userRoles() {
    	return $this->hasMany('UserRoles', 'user_id')
            		->join('role', 'user_role.role_id','=', 'role.id');
  	}
	
}
