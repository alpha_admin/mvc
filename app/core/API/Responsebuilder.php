<?php
/*
 * Class Responsebuilder
 * Forms jason for api response
 * builder()- returns json header response
 * 
 * @author Arghya Ghosh <arghya@capitalnumbers.com>
 * @date July 20, 2016
 */

abstract class Responsebuilder
{
	public function builder($statusCode = 404, $message = NULL, $responseData = NULL , $pagination = array())
	{
		$response = array(
			"status" => $statusCode,
            "message" => $message,
            "data" => ($statusCode == '200' ? $responseData : NULL)
		);
		$jsonDataResponse = json_encode($response);
		header('Content-Type: application/json');
		die($jsonDataResponse);
	}
}