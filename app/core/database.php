<?php
/*
 * DB configurations goes here in this file
 * 
 * @author Arghya Ghosh <arghya@capitalnumbers.com>
 * @date Sept 05, 2016
 */
use Illuminate\Database\Capsule\Manager as Capsule;	

$capsule = new Capsule();

$capsule->addConnection(
	[
		'driver' => _DB_DRIVER_,
		'host' => _DB_HOST_,
		'username' => _DB_USERNAME_,
		'password' => _DB_PASSWORD_,
		"database" => _DB_NAME_,
		"charset" => _DB_CHARSET_,
		"collation" => _DB_COLLATION_,
		"prefix" => _DB_TABLE_PREFIX_
	]
);

$capsule->bootEloquent();