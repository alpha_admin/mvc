<?php
class Home extends Controller
{
	protected $user;
	protected $place;
	
	public function __construct() {
		parent::__construct();
	}
	
	public function accessLevel()
	{
		
	}
	
	public function action_index($name = '', $place = '')
	{
		$this->user = $name;
		$this->place = $place;
		$this->view('home/index', ['name' => $this->user, 'place' => $this->place]);
	} 
	
	public function action_contact()
	{
		$this->view('home/_contact');
	}  
	
	public function about()
	{
		$this->view('home/_about');
	}
	
	public function action_test($name = '', $otherName = '') 
	{
		echo $name;
	}
	
	public function action_create($userName = '', $emailId = "")
	{
		$this->user->create(
			[
				'username' => $userName,
				'email'	=> $emailId
			]
		);
	}
	
	public function action_signin() 
	{
		if ($_POST) {
			$this->userAuthentication($_POST);
			$this->redirect("user/index");
		}
		$this->loadFullView('home/_signin');
	}
	
	public function action_signout()
	{
		if ($_SESSION) {
			foreach ($_SESSION as $key => $val) {
				unset($_SESSION["$key"]);
			}
		}
		session_destroy();
		$this->redirect();
	}
	
	public function action_unauthorized()
	{
		$this->view('home/_unauthorized');
	}
	
}