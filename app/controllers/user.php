<?php
/*
 * Class user
 * index()- returns all users
 * fetch()- returns specific user related data
 * 
 * @author Arghya Ghosh <arghya@capitalnumbers.com>
 * @date Sept 05, 2016
 */

class user extends DB_Layer_User
{
	
	public function __construct() {
		parent::__construct();
	}
	
	public function action_fetch($id = '')
	{
		$respose = $this->detailsData($id);
	}
	
	public function action_index($id = '')
	{
		if ($this->appUserId) {
// 			echo "<pre>";
// 			print_r($_SESSION);
// 			echo "</pre>";
			$respose = $this->listData($id);
		} else {
			$this->redirect("home/unauthorized");
		}
	}
}