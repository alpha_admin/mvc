<?php
/**
* Autoloader file
*
*
* @Package IRA
* @author Arghya Ghosh <arghya@capitalnumbers.com>
* 
*/

# auto loads all required configuration files to the app

require_once 'core/config/main.php';

//composer auto loader
require_once '../vendor/autoload.php';

// core files auto loader
require_once 'core/database.php';
require_once 'core/App.php';
require_once 'core/Controller.php';

// API response loader
require_once 'core/API/Responsebuilder.php';

// exception handlers auto loader
require_once 'core/exceptions/EException.php';
require_once 'core/exceptions/ExtendException.php';

// DB Abstraction layer files auto loader
foreach( glob(dirname(__FILE__) . '/manager/*.php') as $class_path ) 
	require_once( $class_path );

// utils loader
foreach( glob(dirname(__FILE__) . '/core/utils/*.php') as $utils_class_path ) 
	require_once( $utils_class_path );

// auth component loader
foreach( glob(dirname(__FILE__) . '/core/auth/*.php') as $auth_class_path ) 
	require_once( $auth_class_path );

// models loader
foreach( glob(dirname(__FILE__) . '/models/*.php') as $models_class_path )
require_once( $models_class_path );

// starting session;
session_start();
