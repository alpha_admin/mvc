<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>IRA</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo APP_URL . "theme/bootstrap/dist/css/bootstrap.min.css";?>" type="text/css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo APP_URL . "theme/bootstrap/docs/assets/css/ie10-viewport-bug-workaround.css";?>" type="text/css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo APP_URL . "theme/bootstrap/docs/examples/justified-nav/justified-nav.css";?>" type="text/css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="<?php echo APP_URL . "theme/bootstrap/docs/assets/js/ie8-responsive-file-warning.js";?>"></script><![endif]-->
    <script src="<?php echo APP_URL . "theme/bootstrap/docs/assets/js/ie-emulation-modes-warning.js";?>"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <!-- The justified navigation menu is meant for single line per list item.
           Multiple lines will require custom code not provided by Bootstrap. -->
      <div class="masthead">
        <h3 class="text-muted"><?php echo _SITE_NAME_;?></h3>
        <nav>
          <ul class="nav nav-justified">
            <li<?php if (strstr($_SERVER['REQUEST_URI'], 'home/index')) { ?>  class="active" <?php }?>><a href="<?php echo APP_URL . "home/index";?>">Home</a></li>
            <li<?php if (strstr($_SERVER['REQUEST_URI'], 'home/about')) { ?>  class="active" <?php }?>><a href="<?php echo APP_URL . "home/about";?>">About</a></li>
            <li<?php if (strstr($_SERVER['REQUEST_URI'], 'home/contact')) { ?>  class="active" <?php }?>><a href="<?php echo APP_URL . "home/contact";?>">Contact</a></li>
            <?php if (!$this->appUserId) {?>
            	<li><a href="<?php echo APP_URL . "home/signin";?>">Sign in</a></li>
            <?php }?>
            <?php if ($this->appUserId) {?>
            	<li><a href="<?php echo APP_URL . "home/signout";?>">Log out</a></li>
            <?php }?>
          </ul>
        </nav>
      </div>